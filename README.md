# GameMaker Macros #

This repo will host GameMaker : Studio macros that I have found useful. All macros are written in python 3.x.

# License #

[WTFPL](http://www.wtfpl.net/about/)