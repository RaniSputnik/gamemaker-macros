# This command is used to clean a GameMaker : Studio project of all the cruft
# If you are like me and didn't know about the preference;
#      "Deletes from resource tree deletes from disk"
# It's likely that you have a bunch of resources in your project that you aren't
# using. You can clean those with this script
# 
# Usage as follows;
# python clean-project <flags> <project>
#    -s | --safe : Don't modify the project, the command will simply print out the files it
#				   would delete if run without the safe flag.
# Eg.
# python clean-project ../projects/my-gms-project.gmx
# python -s clean-project ./cool-game.gmx

import os, glob, zipfile, sys;
import xml.etree.ElementTree as ET

if len(sys.argv) < 2:
	raise Exception("You must specify a project to clean")

# Process Command Line Args
flag_safe = False
for flag in sys.argv[1:-1]:
	if flag == "--safe" or flag == "-s":
		flag_safe = True
		print("Reading in safe mode, your project files will not be modified")
arg_project = sys.argv[-1]

# Determine the project directory
project_dir = os.path.normpath(os.path.join(os.getcwd(), arg_project))
if os.path.isdir(project_dir):
	matches = glob.glob(os.path.join(project_dir,"*.project.gmx"))
	if len(matches) == 0:
		raise Exception("Specified project directory does not contain a .project.gmx file!\n"+project_dir)
	else:
		project_file = os.path.join(project_dir,matches[0])
else: 
	raise Exception("Specified project directory is not a directory!\n"+project_dir)
project_dir = os.path.normpath(project_dir)
project_file = os.path.normpath(project_file)

def read_xml(dir):
	file = open(dir,"r")
	contents = file.read()
	file.close()
	return ET.fromstring(contents)	

def remove_unused_files(glob_pattern,used_files):
	files = glob.glob(glob_pattern)
	for file in files:
		file_name = os.path.basename(file)
		if file_name not in used_files:
			if flag_safe:
				print("Will remove "+file_name)
			else:
				os.remove(file)
				print("Removed "+file_name)
	
# Read all the scripts from the project
project_xml = read_xml(project_file)
project_scripts = []
print("Reading scripts from project file...")
for scripts in project_xml.iter("scripts"):
	for script in scripts.iter("script"):
		script_path = os.path.join(project_dir,script.text)
		script_name_and_ext = os.path.basename(script_path)
		script_name = os.path.splitext(script_name_and_ext)[0]
		project_scripts.append(script_name+".gml")

print("Removing unused scripts from project...")
scripts_glob = os.path.join(project_dir,"scripts","*.gml")
remove_unused_files(scripts_glob,project_scripts)	

# Read all the sprites from the project
project_sprites = []
print("Reading sprites from project file...")
for sprites in project_xml.iter("sprites"):
	for sprite in sprites.iter("sprite"):
		sprite_path = os.path.join(project_dir,sprite.text+".sprite.gmx")
		sprite_name = os.path.basename(sprite_path)
		project_sprites.append(sprite_name)

print("Removing unused sprites from project....")
sprites_glob = os.path.join(project_dir,"sprites","*.sprite.gmx")
remove_unused_files(sprites_glob,project_sprites)
	
print("Reading sprite frames from project file...")
sprite_frames = []
for project_sprite in project_sprites:
	sprite_path = os.path.join(project_dir,"sprites",project_sprite)
	sprite_xml = read_xml(sprite_path)
	for frame in sprite_xml.iter("frame"):
		frame_name = os.path.basename(frame.text)
		sprite_frames.append(frame_name)

sprite_frames_glob = os.path.join(project_dir,"sprites","images","*")
remove_unused_files(sprite_frames_glob,sprite_frames)
	
print("Done :)")